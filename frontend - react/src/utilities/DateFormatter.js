class Point {

    static format01(date) {
        const srcDate = new Date(date)

        const format_hours = srcDate.getHours() > 12 ? srcDate.getHours() - 12 :srcDate.getHours() ;
        const am_pm = srcDate.getHours() < 12 ? 'AM' :'PM' ;
        const hours = format_hours < 10 ? '0' + format_hours :format_hours ;
        const minutes = srcDate.getMinutes() < 10 ? '0' + srcDate.getMinutes() :srcDate.getMinutes() ;        
        const formattedDate = hours +':' + minutes + ' ' + am_pm;

        return formattedDate;
    }
}

export default Point;