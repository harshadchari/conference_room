import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import Login from '../Login';

function PrivateRoute({ children, ...rest }) {
    return (
        <Route
            {...rest}
            render={(props) => {               

                return (rest.isLoggedIn===true) ? (children) : (
                    <Login />
                );
            }}
        />
    )
}

export default PrivateRoute;