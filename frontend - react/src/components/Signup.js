import React, { Component } from 'react'

import {Link } from 'react-router-dom'
import Axios from 'axios';

import { config } from './../config/Constants'

 class Signup extends Component {
    state = {
        auth: {
            email: "",
            password: ""
        },
        iserror: false
    }
    
    onHandleChange = (e) => {
        this.setState({
            auth: {
                ...this.state.auth,
                [e.target.name]: e.target.value
            }

        });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        Axios.post(config.url.API_URL + '/api/user/signup', this.state.auth)
            .then(res => {
                
                this.props.history.push("/login");
            })
            .catch(err => {
                this.setState({ iserror: true })
            });

    }
    render() {
        let { iserror } = this.state;
        const renderError = iserror ? (
            <div className="alert alert-danger">
                <a  className="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Error!</strong> Invalid credentials
          </div>
        ) : null
        return (
            <div className="container-fluid">
                <div className="row">
                    <div style={styleSignupPanel} className="col-xs-10 col-xs-offset-1 col-md-4 col-md-offset-4 well">
                        <h1>Sign-Up</h1>
                        <hr />
                        <form onSubmit={this.handleSubmit}>
                            <div className="form-group">
                                <input className="form-control" onChange={this.onHandleChange} type="email" name="email" id="email" placeholder="Email" />
                            </div>
                            <div className="form-group">
                                <input className="form-control" type="password" onChange={this.onHandleChange} name="password" id="password" placeholder="Password" />
                            </div>
                            <div className="form-group text-center">
                                <button type="submit" className="btn btn-success">Sign up</button>
                            </div>

                        </form>
                        {renderError}
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-4 col-xs-offset-4 ">
                        <p className="text-center">Existing User ? <Link className="text-center" to="/login">Login</Link></p>
                    </div>
                </div>
            </div>
        )
    }
}

const styleSignupPanel = {
    padding: "25px",
    marginTop: "25vh"
};

export default Signup;
