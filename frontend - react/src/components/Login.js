import React, { Component } from 'react'
import {Link } from 'react-router-dom'
import Axios from 'axios';

import { config } from './../config/Constants'

class Login extends Component {
    state = {
        auth: {
            email: "",
            password: ""
        },
        iserror: false
    }

    onHandleChange = (e) => {
        this.setState({
            auth: {
                ...this.state.auth,
                [e.target.name]: e.target.value
            }

        });
    }

    handleSubmit = (e) => {
               
        e.preventDefault();
        Axios.post(config.url.API_URL + '/api/user/login', this.state.auth)
            .then(res => {
                console.log(res.data);
                localStorage.setItem("token",res.data.token);
                localStorage.setItem("userId",res.data.userId);
                this.props.setLogin(true);
                this.props.history.push("/");
            })
            .catch(err => {
                this.setState({ iserror: true })
            });

    }
    render() {
        let { iserror } = this.state;
        const renderError = iserror ? (
            <div className="alert alert-danger">
                <a href="#" className="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Error!</strong> Invalid Login email or password
          </div>
        ) : null
        return (
            <div className="container-fluid">
                <div className="row">
                    <div style={styleLoginPanel} className="col-xs-10 col-xs-offset-1 col-md-4 col-md-offset-4 well">
                        <h1>Login</h1>
                        <hr />
                        <form onSubmit={this.handleSubmit}>
                            <div className="form-group">
                                <input className="form-control" onChange={this.onHandleChange} type="email" name="email" id="email" placeholder="Email" />
                            </div>
                            <div className="form-group">
                                <input className="form-control" type="password" onChange={this.onHandleChange} name="password" id="password" placeholder="Password" />
                            </div>
                            <div className="form-group text-center">
                                <button type="submit" className="btn btn-success">Login</button>
                            </div>

                        </form>
                        {renderError}
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-4 col-xs-offset-4 ">
                        <p className="text-center">New User ? <Link className="text-center" to="/signup">SignUp</Link></p>
                    </div>
                </div>
            </div>
        )
    }
}

const styleLoginPanel = {
    padding: "25px",
    marginTop: "25vh"
};

export default Login;

