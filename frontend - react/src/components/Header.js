import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import {
    withRouter
} from 'react-router-dom'

class Header extends Component {

    logout = () => {
        localStorage.removeItem("userId");
        localStorage.removeItem("token");
        this.props.logout();
        this.props.history.push("/login");
    }
    render() {
        return (
            <div>
                <nav className="navbar navbar-default">
                    <div className="container-fluid">
                        <div className="navbar-header">
                            <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span className="sr-only">Toggle navigation</span>
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                            </button>
                            <Link to="/" className="navbar-brand">
                                ConRooms
                            </Link>
                        </div>
                        <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">


                            <ul className="nav navbar-nav navbar-right">
                                <li><button onClick={this.logout} type="button" className="btn btn-default navbar-btn">Logout</button></li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div >
        )
    }
}

export default withRouter(Header);