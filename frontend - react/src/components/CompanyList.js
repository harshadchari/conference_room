import React, { Component } from 'react'
import Axios from 'axios';

import { config } from './../config/Constants'
import Point from './../utilities/DateFormatter'

class CompanyList extends Component {
    state = {
        companies: [],
        isLoading: false
    }



    componentDidMount() {
        this.setState({ isLoading: true });
        const token = localStorage.getItem("token");
        Axios.get(config.url.API_URL + '/api/companies', {
            headers: {
                Authorization: 'Bearer ' + token
            }
        })
            .then(res => {
                console.log(res.data.companies);

                this.setState({
                    companies: res.data.companies,
                    isLoading: false
                });

            })
    }
    render() {

        const companyList = this.state.companies.map((company) => {
            const startTime = Point.format01(company.dayStartTime);
            const endTime = Point.format01(company.dayEndTime);


            return (
                <div className="col-xs-12 col-sm-6 col-md-3" key={company._id}>
                    <div className="panel panel-default">
                        <div style={panelHeadingStyle} className="panel-heading">{company.name}</div>
                        <div className="panel-body">
                            <table style={tableStyle} className="table ">
                                <tbody>
                                    <tr>
                                        <td>Start</td>
                                        <td>{startTime}</td>
                                    </tr>
                                    <tr>
                                        <td>End</td>
                                        <td>{endTime}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div className="panel-footer">
                            <button className="btn btn-xs btn-primary">Rooms</button>
                            <button className="btn btn-xs pull-right btn-success">Book</button>
                            <span className="clearfix"></span>
                        </div>
                    </div>
                </div>
            )
        })

        const content = this.state.isLoading ? (
            <p>Data is Loading...</p>
        ) : companyList

        return (
            <div className="container">
                <div className="row">
                    <div className="col-xs-12">
                        <h1>Companies</h1>
                        <hr />
                    </div>                    
                </div>
                <div className="row">
                    {content}
                </div>
            </div>
        )
    }
}

const tableStyle = {
    marginBottom: "0"
}

const panelHeadingStyle = {
    fontSize: "2.5rem"
}
export default CompanyList;