import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom'

import 'jquery/src/jquery';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min';

import Header from './components/Header'
import Login from './components/Login';
import CompanyList from './components/CompanyList';
import Signup from './components/Signup';
import PrivateRoute from './components/helpers/PrivateRoute'

class App extends Component {

  componentDidMount() {
    if (localStorage.getItem("userId") != null) {
      this.setState({
        isLoggedIn: true
      });
    }
  }
  state = {
    isLoggedIn: false
  }

  setLogin = (isLoggedIn) => {
    this.setState({
      isLoggedIn: isLoggedIn
    })
  }

  logout = () => {
    this.setLogin(false);
  }

  render() {
    let header = this.state.isLoggedIn ? <Header logout={this.logout} /> : null;

    return (
      <Router>
        <div className="App">
          {header}
          {/* <Route exact path="/" component={CompanyList} /> */}
          <Route
            path="/login"
            render={(props) =>
              <Login  {...props} setLogin={this.setLogin} />}
          />
          <Route
            path="/signup"
            render={(props) => <Signup  {...props} />}
          />
          <PrivateRoute isLoggedIn={this.state.isLoggedIn} exact path="/">
            <CompanyList />
          </PrivateRoute>
        </div>
      </Router>
    );
  }

}


export default App;
