import { Component, OnInit } from '@angular/core';
import { Company } from '../company.model';
import { CompanyService } from '../company.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-company',
  templateUrl: './add-company.component.html',
  styleUrls: ['./add-company.component.css']
})
export class AddCompanyComponent implements OnInit {


  companyModel: Company = { id: null, name: '', dayStartTime: '', dayEndTime: '', content: '' };
  constructor(public companyService: CompanyService, public router: Router) { }

  ngOnInit(): void {
  }

  onSubmit() {
    console.log(this.companyModel);
    this.companyService.addCompany(this.companyModel)
      .subscribe((responseData) => {
        console.log(responseData);

        this.router.navigate(['/']);

      })
  }

}
