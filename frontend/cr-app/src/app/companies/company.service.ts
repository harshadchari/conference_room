import { Injectable } from '@angular/core';
import { Company } from './company.model';
import { HttpClient } from '@angular/common/http';

import { Subject } from 'rxjs';
import { map } from 'rxjs/operators'
import { Room } from '../rooms/room.model';
import { environment } from './../../environments/environment';

@Injectable({ providedIn: 'root' })
export class CompanyService {
  private companies: Company[] = [];
  private companiesUpdated = new Subject<Company[]>();

  private companySelected = new Subject<{ rooms: Room[], companyName: string }>();

  private selectedCompany: Company;

  constructor(private http: HttpClient) { }

  getPosts() {
    this.http.get<{ message: string, companies: any }>(environment.apiUrl + '/api/companies')
      .pipe(map((companyData) => {
        return companyData.companies.map(company => {
          return {
            name: company.name,
            dayStartTime: company.dayStartTime,
            dayEndTime: company.dayEndTime,
            id: company._id
          };
        });
      })
      )
      .subscribe((transformedCompanies) => {
        this.companies = transformedCompanies;
        this.companiesUpdated.next([...this.companies]);
      });
  }

  getCompanyUpdatedListener() {
    return this.companiesUpdated.asObservable();
  }

  getCompanySelecteddListener() {
    return this.companySelected.asObservable();
  }

  showCompanyDetails(company) {
    this.companySelected.next(company);
  }

  getRoomBookings(roomId: string) {
    return this.http.get<{ message: string, booking: any }>(environment.apiUrl + '/api/bookings/book/' + roomId)

  }

  showCompanyRooms(company) {

    this.http.get<{ message: string, rooms: any }>(environment.apiUrl + '/api/rooms/' + company.id)
      .subscribe((data) => {
        this.selectedCompany = company;
        this.companySelected.next({ rooms: [...data.rooms], companyName: company.name });


      }, err => {
        this.companySelected.next({ rooms: [], companyName: company.name });
      });


  }

  addCompany(company: Company) {
    return this.http.post<{ message: string, companyId: string }>(environment.apiUrl + '/api/companies/', company);

  }

}
