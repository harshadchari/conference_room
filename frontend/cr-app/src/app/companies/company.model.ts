export interface Company {
  id: string;
  name: string;
  dayStartTime: string;
  dayEndTime: string;
  content: string;
}
