import { Component, OnInit } from '@angular/core';
import { Company } from '../company.model';
import { Subscription } from 'rxjs';
import { CompanyService } from '../company.service';
import { MatDialog } from '@angular/material/dialog';
import { AddCompanyComponent } from '../add-company/add-company.component';

@Component({
  selector: 'app-company-list',
  templateUrl: './company-list.component.html',
  styleUrls: ['./company-list.component.css']
})
export class CompanyListComponent implements OnInit {
  companies: Company[] = [];
  private companiesSub: Subscription;

  isLoading = false;

  constructor(public companyService: CompanyService, public dialog: MatDialog) { }

  ngOnInit() {
    this.isLoading = true;
    this.companyService.getPosts();
    this.companiesSub = this.companyService.getCompanyUpdatedListener()
      .subscribe((companies: Company[]) => {
        this.isLoading = false;
        this.companies = companies;
      });
  }

  onDelete(postId: string) {
    //this.companyService.deletePost(postId);
  }

  addCompany() {


    const dialogRef = this.dialog.open(AddCompanyComponent, {
      width: '500px',
      data: { value:"n" }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');

    });
  }

  ngOnDestroy() {
    this.companiesSub.unsubscribe();
  }

  onSelectCompany(company) {
    this.companyService.showCompanyRooms(company);

  }
}
