import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Booking, Slot } from '../booking.model';
import { BookingService } from './booking.service';
import { Room } from 'src/app/rooms/room.model';
import { RoomListComponent } from 'src/app/rooms/room-list/room-list.component';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css']
})
export class BookingComponent implements OnInit {

  constructor(public bookingService: BookingService, public route: ActivatedRoute) { }

  private companyName: string;
  bookingModel = new Booking(null, '', new Slot('', ''), 0, false, false);

  submitted = false;
  responseReceived = false;
  roomsFound = false;

  public bookedRoom: Room = { id: null, name: '', maxCapacity: 0, hasProjector: false, hasTelephone: false };
  public savedBooking: Booking = new Booking(null, '', new Slot('', ''), 0, false, false);
  onSubmit() {
    console.log("submitting");
    this.submitted = true;
    this.bookingService.addBooking(this.bookingModel, this.companyName)
      .subscribe((responseData) => {
        console.log(responseData);
        if (responseData.status) {
          const bookingId = responseData.booking.id;
          this.bookingModel.id = bookingId;

          console.log("booked successfully");
          this.bookedRoom = responseData.room;
          this.savedBooking = responseData.booking;
          this.roomsFound = true;
        } else {
          console.log("err");
          this.roomsFound = false;
        }
        this.responseReceived = true;
      });

  }


  ngOnInit(): void {
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has('name')) {
        this.companyName = paramMap.get('name');
        console.log(this.companyName);

        // this.isLoading = true;
        // this.postsService.getPost(this.postId).subscribe(postData => {
        //   this.isLoading = false;
        //   this.post = {
        //     id: postData._id,
        //     title: postData.title,
        //     content: postData.content
        //   };
        //   this.form.setValue({
        //     'title': this.post.title,
        //     'content': this.post.content
        //   });
        // });
      } else {
        // this.mode = 'create';
      }
    });
  }

}
