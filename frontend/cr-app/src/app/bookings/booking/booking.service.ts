import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Booking } from '../booking.model';
import { Observable, of } from 'rxjs';
import { Room } from 'src/app/rooms/room.model';
import { environment } from './../../../environments/environment';

@Injectable({ providedIn: 'root' })
export class BookingService {

  constructor(private http: HttpClient) { }

  addBooking(booking: Booking, companyName: string) {

   return this.http.post<{ status:string,message: string, booking: Booking,room:Room }>(environment.apiUrl + '/api/bookings/book/' + companyName, booking)


  }


}
