import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../auth.service';
import { Subscription } from 'rxjs';
import { NgForm } from '@angular/forms';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit,OnDestroy {
  constructor(public authService: AuthService) { }

  isLoading = false;
  private authStatusSub:Subscription;

  onLogin(form:NgForm){
    if(form.invalid){
      return
    }
    this.isLoading = true;
    this.authService.login(form.value.email,form.value.password);
  }

  ngOnInit(): void {
    this.authStatusSub = this.authService.getAuthStatusListener()
      .subscribe(authStatus => {
        this.isLoading = false;
      });

  }

  ngOnDestroy(): void {
    this.authStatusSub.unsubscribe();
  }

}
