import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { CompanyService } from './../../companies/company.service';
import { Room } from '../room.model';

@Component({
  selector: 'app-room-list',
  templateUrl: './room-list.component.html',
  styleUrls: ['./room-list.component.css']
})
export class RoomListComponent implements OnInit, OnDestroy {

  public rooms: Room[] = [];
  private compSub: Subscription;
  public  companyName;
  constructor(public companyService: CompanyService) { }

  ngOnInit(): void {
    this.compSub = this.companyService.getCompanySelecteddListener()
      .subscribe(({ rooms, companyName }) => {
        this.rooms = rooms;
        this.companyName = companyName;
      });
  }

  ngOnDestroy() {
    this.compSub.unsubscribe();
  }

}
