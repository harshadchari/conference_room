import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { CompanyComponent } from 'src/app/companies/company.component';
import { BookingComponent } from './bookings/booking/booking.component';
import { RoomListComponent } from './rooms/room-list/room-list.component';
import { BookingListComponent } from './bookings/booking-list/booking-list.component';
import { LoginComponent } from './auth/login/login.component';
import { SignupComponent } from './auth/signup/signup.component';
import { AuthGuard } from './auth/auth.guard';

const routes: Routes = [
  { path: '', component: CompanyComponent,canActivate: [AuthGuard] },
  { path: 'bookings/:name', component: BookingComponent,canActivate: [AuthGuard] },
  { path: 'room/bookings/:roomId', component: BookingListComponent ,canActivate: [AuthGuard]},
  { path: 'user/login', component: LoginComponent },
  { path: 'user/signup', component: SignupComponent }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers:[AuthGuard]
})

export class AppRoutingModule { }
