import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';

import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";

import { AppComponent } from './app.component';
import { CompanyListComponent } from './companies/company-list/company-list.component';
import { HeaderComponent } from './header/header.component';
import { CompanyComponent } from './companies/company.component';
import { RoomListComponent } from './rooms/room-list/room-list.component';
import { AppRoutingModule } from './app-routing.module';
import { BookingComponent } from './bookings/booking/booking.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

/** Angular Material */
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner'
import { MatCardModule } from '@angular/material/card';

import { MatDialogModule } from '@angular/material/dialog';
import {MatSnackBarModule} from '@angular/material/snack-bar';

import { BookingListComponent } from './bookings/booking-list/booking-list.component';
import { LoginComponent } from './auth/login/login.component';
import { SignupComponent } from './auth/signup/signup.component';
import { ErrorComponent } from './error/error.component';

import { AuthInterceptor } from './auth/auth.interceptor';
import { ErrorInterceptor } from './error-interceptor';
import { AddCompanyComponent } from './companies/add-company/add-company.component';

@NgModule({
  declarations: [
    AppComponent,
    CompanyListComponent,
    HeaderComponent,
    CompanyComponent,
    RoomListComponent,
    BookingComponent,
    BookingListComponent,
    LoginComponent,
    SignupComponent,
    ErrorComponent,
    AddCompanyComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatCardModule,
    MatDialogModule,
    MatSnackBarModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
