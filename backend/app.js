const express = require('express');
const bodyParser = require("body-parser");

//connect to database
require('./database')

const app = express();

const companyRoutes = require('./routes/company');
const roomRoutes = require('./routes/room');
const bookingRoutes = require('./routes/booking');
const userRoutes = require('./routes/user');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader(
        "Access-Control-Allow-Headers",
        "Origin,X-Requested-With,Content-Type,Accept,Authorization"
    );
    res.setHeader(
        "Access-Control-Allow-Methods",
        "GET,POST,PATCH,DELETE,PUT,OPTIONS"
    );
    next();
});


//Routes
app.use("/api/companies",companyRoutes);
app.use("/api/rooms",roomRoutes);
app.use("/api/bookings",bookingRoutes);
app.use("/api/user",userRoutes);

module.exports = app;

