const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Booking = require('./booking.model')

const roomSchema = mongoose.Schema({
    name: { type: String, required: true },
    company_id: { type: mongoose.ObjectId, required: true },
    maxCapacity: { type: Number, required: true },
    hasProjector: { type: Boolean, required: true },
    hasTelephone: { type: Boolean },
    bookings:[{ type: Schema.Types.ObjectId, ref: Booking }]
});

module.exports = mongoose.model('Room', roomSchema, 'room');

