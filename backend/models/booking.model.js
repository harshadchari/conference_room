const mongoose = require('mongoose');

const bookingSchema = mongoose.Schema({
    room_id: { type: mongoose.ObjectId, required: true },
    date: { type: String, required: true },
    slot: {
        startTime: { type: String, required: true },
        endTime: { type: String, required: true }
    },
    capacity: { type: Number },
    needsTelephone: { type: Boolean },
    needsProjector: { type: Boolean },
});

module.exports = mongoose.model('Booking', bookingSchema, 'booking');

