const mongoose = require('mongoose');

const companySchema = mongoose.Schema({
    name: { type: String, required: true },
    dayStartTime: { type: Date, required: true },
    dayEndTime: { type: Date, required: true },
    fieldPriorities: { type: [String] }
});

module.exports = mongoose.model('Company', companySchema,'company');
