require('dotenv').config()
const config = require('./config');

const { db: { host, port, name, user, password } } = config;

let connectionString = ''
let envType = ''
if (process.env.NODE_ENV === "dev") {
    connectionString = `mongodb://${host}:${port}/${name}`
    envType = 'development';
} else if (process.env.NODE_ENV === "prod") {
    connectionString = `mongodb+srv://${user}:${password}@${host}/${name}?retryWrites=true&w=majority`
    envType = 'production';
} else {
    console.log("Incorrect Dev ENV");
}

module.exports = {
    CONNECTION_STRING: connectionString,
    environment:envType
}