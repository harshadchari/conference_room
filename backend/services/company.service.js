const Company = require('../models/company.model')

class CompanyService { }

CompanyService.getByName = async (name) => {
    const companies = await Company.find({ name: name })
    let result;

    if (companies.length > 0) {
        result = {
            found: true,
            company: companies[0]
        }
    } else {
        result = {
            found: false
        }
    }
    console.log(result);

    return result;
}

CompanyService.add = async (company) => {

    const savedResult = await company.save()
        .then(result => {
            return {
                status: true,
                message: 'Company Added successfully',
                company_Id: result._id
            };
        })
        .catch((err) => {
            console.log(err);
            
            return {
                status: false,
                message: 'Company Add Failed',
                error: err.errmsg
            };
        });

    return savedResult;
}
CompanyService.getAll = async () => {

    const result = await Company.find()
        .then(result => {
            return {
                status: true,
                companies: result
            };
        })
        .catch((err) => {
            return {
                status: false,
                error: err.errmsg
            };
        });

    return result;
}

module.exports = CompanyService;