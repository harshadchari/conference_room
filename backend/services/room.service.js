const Room = require('../models/room.model');

const CompanyService = require('../services/company.service');

class RoomService { }

RoomService.getAll = async () => {
    const rooms = await Room.find()
    return {
        "message": 'Rooms fetched successfully',
        rooms
    }
}

RoomService.getByCompanyId = async (company_id) => {
    const rooms = await Room.find({
        company_id: { $eq: company_id }
    })
    console.log(rooms);
    return {
        "message": 'Rooms fetched successfully',
        rooms
    }
}

RoomService.save = async (company_name, roomDTO) => {
    let result;
    let company = await CompanyService.getByName(company_name);
    if (company.found) {
        // save room
        console.log(company.company);

        const room = new Room({
            name: roomDTO.name,
            company_id: company.company._id,
            maxCapacity: roomDTO.maxCapacity,
            hasProjector: roomDTO.hasProjector,
            hasTelephone: roomDTO.hasTelephone
        });

        const roomRes = await room.save();
        result = {
            status: true,
            room: roomRes
        }

    } else {
        result = {
            status: false,
            message: "Company Not Found"
        }
    }
    return result;
}

RoomService.addBooking = async (roomId, booking) => {
    let result;
    Room.findByIdAndUpdate(
        roomId,
        { $push: { bookings: booking } },
        { new: true }, (err, doc) => {
            if (doc != null) {
                result = {
                    status: true,
                    message: "Room Bookings Updated.",
                    company: doc
                }
            } else {
                result = {
                    status: false,
                    message: "Room Bookings Not Updated."
                }
            }
        }
    );

    return result;
}


RoomService.getByBooking = async (company_id, bookingRequest) => {
console.log(company_id);

    var where = {
        maxCapacity: { $gte: bookingRequest.capacity },
        company_id: { $eq: company_id }
    }
    console.log(bookingRequest);

    if (bookingRequest.needsProjector) {
        where = { ...where, hasProjector: true };
    }

    if (bookingRequest.needsTelephone) {
        where = { ...where, hasTelephone: true };
    }

    const rooms = await Room.find(where)
        .sort({ maxCapacity: 'asc' })
        .populate({
            path: 'bookings',
            match: {
                "date": { $eq: bookingRequest.date },
                $or: [
                    {
                        $and: [
                            { "slot.startTime": { $lte: bookingRequest.slot.startTime } },
                            { "slot.endTime": { $gt: bookingRequest.slot.startTime } }
                        ]
                    },
                    {
                        $and: [
                            { "slot.startTime": { $lt: bookingRequest.slot.endTime } },
                            { "slot.endTime": { $gte: bookingRequest.slot.endTime } }
                        ]
                    },
                    {
                        $and: [
                            { "slot.startTime": { $eq: bookingRequest.slot.startTime } },
                            { "slot.endTime": { $eq: bookingRequest.slot.endTime } }
                        ]
                    }
                ]
            },
        });
    let result;

    if (rooms.length > 0) {
        result = {
            status: true,
            rooms: rooms
        }
    } else {
        result = {
            status: false
        }        
    }
    return result;


}
module.exports = RoomService;