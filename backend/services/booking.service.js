const Booking = require('../models/booking.model');

const CompanyService = require('./company.service')
const RoomService = require('./room.service')

class BookingService { }

BookingService.add = async (company_name, bookingRequest) => {
    let result;
    let room;
   //console.log(company_name);

    let companyResult = await CompanyService.getByName(company_name);

    if (companyResult.found) {        
        
        //find rooms matching requirements sorted in asc maxCapacity
        let roomResult = await RoomService.getByBooking(companyResult.company._id, bookingRequest);

        if (roomResult.status) {
            //choose lowest capacity room
            let choosenRoom;
            let foundRoom = false;
            for (var i = 0; i < roomResult.rooms.length; i++) {
                if (roomResult.rooms[i].bookings.length == 0) {
                    choosenRoom = roomResult.rooms[i];
                    foundRoom = true;
                    break;
                }
            }
            if (foundRoom) {
                // book room
                const booking = new Booking({
                    room_id:choosenRoom.id,                                       
                    date: bookingRequest.date,
                    slot: {
                        startTime: bookingRequest.slot.startTime,
                        endTime: bookingRequest.slot.endTime
                    },
                    capacity: bookingRequest.capacity,
                    needsTelephone: bookingRequest.needsTelephone,
                    needsProjector: bookingRequest.needsProjector
                });


                //save booking
                const savedBooking = await booking.save();


                //save reference to booking in room
                const updatedRoom = await RoomService.addBooking(choosenRoom._id, savedBooking);

                result = {
                    status: true,
                    booking: savedBooking,
                    room : {
                        id: choosenRoom._id,
                        name:choosenRoom.name,
                        maxCapacity:choosenRoom.maxCapacity,
                        hasProjector:choosenRoom.hasProjector,
                        hasTelephone:choosenRoom.hasTelephone
                        
                    },
                    message: "Booking Saved Successfully."
                }
            } else {
                result = {
                    status: false,
                    message: "No Rooms without confilcts Found."
                }
            }

        } else {
            result = {
                status: false,
                message: "No Rooms matching requirements Found."
            }
        }

    } else {
        result = {
            status: false,
            message: "Company Not Found"
        }
    }
    return result;
}

BookingService.getBookingsByRoom = async (room_id) => {
    const bookings = await Booking.find({
        room_id: { $eq: room_id }
    })

    return {
        
        "message": 'Bookings fetched successfully',
        bookings
    }
}

module.exports = BookingService;