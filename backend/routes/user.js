
const express = require("express");
const router = express.Router();

const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const User = require("../models/user.model");

/**
 * Endpoint : User Signup 
 * @param {string} req.body.email
 * @param {string} req.body.password
 */
router.post("/signup", (req, res, next) => {
    bcrypt.hash(req.body.password, 10)
        .then(hash => {
            const user = new User({
                email: req.body.email,
                password: hash
            });
            user.save()
                .then(result => {
                    res.status(200).json({
                        message: 'User Created',
                        result: result
                    })
                })
                .catch(err => {
                    res.status(500).json({
                        message: 'Invalid Signup Authentication Credentials'
                    });
                });// ./then-user.save

        })// ./then - hash

})// ./router.post-signup


router.post("/login", (req, res, next) => {
    let fetchedUser;
    User.findOne({ email: req.body.email })
        .then(user => {
            if (!user) {
                return res.status(401).json({
                    message: 'Auth Failed'
                });
            }
            fetchedUser = user;
            return bcrypt.compare(req.body.password, user.password)
        })
        .then(result => {
            if (!result) {
                return res.status(401).json({
                    message: 'Invalid Authentication Credentials'
                });
            }

            const token = jwt.sign({ email: fetchedUser.email, userId: fetchedUser._id },
                'secret_this_should_be_longer',
                {
                    expiresIn: "1h"
                });
            res.status(200).json({
                token: token,
                expiresIn: 3600,
                userId: fetchedUser._id
            })
        })
        .catch(err => {
            return res.status(401).json({
                message: 'Auth Failed',
                error: err
            });
        }); // ./ then - user.findOne()

}) // ./router.post-login

module.exports = router;
