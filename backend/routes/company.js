const express = require("express");

const router = express.Router();

const Company = require('./../models/company.model');
const CompanyService = require('../services/company.service');


/**
 * Add a company 
 */
router.post("", async (req, res, next) => {

    const company = new Company({
        name: req.body.name,
        dayStartTime: req.body.dayStartTime,
        dayEndTime: req.body.dayEndTime,
        fieldPriorities: req.body.fieldPriorities
    });

    let result = await CompanyService.add(company);
    if (result.status) {
        res.status(200).json({
            message: result.message,
            companyId: result.company_Id
        });
    } else {
        res.status(404).json({
            message: "Company Not Added",
            error:result.error
        });
    }


});


/**
 * Get all companies 
 */
router.get('',async (req, res, next) => {
    let result = await CompanyService.getAll();
   
    if (result.status) {
        res.status(200).json({
            message: 'Companies fetched successfully',
            companies: result.companies
        });
    } else {
        res.status(500).json({
            message: 'Something went wrong'
        });
    };
});


/**
 * get company by name
 */
router.get('/:name', async (req, res, next) => {
    const name = req.params.name;

    let result = await CompanyService.getByName(name);
    if (result.found) {
        res.status(200).json({
            message: "Company Found",
            company: result.company
        });
    } else {
        res.status(404).json({
            message: "Company Not Found"
        });
    }
});

/**
 * update fieldPriorities by company name
 */

router.put("/fieldPriorities/:name", (req, res, next) => {
    name = req.params.name;
    fieldPriorities = req.body;

    Company.findOneAndUpdate(
        { name: name },
        { fieldPriorities: fieldPriorities },
        { new: true }, (err, doc) => {
            if (doc != null) {
                res.status(200).json({
                    message: "Company Updated",
                    company: doc
                })
            } else {
                res.status(404).json({
                    message: "Company Not Found"
                })
            }
        }
    );



});

module.exports = router;